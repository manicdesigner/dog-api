# To Do File 

- [ ] create markup
- [ ] add dummy API
- [ ] style items in layout.
- [ ] build out mobile layout.
- [ ] build out tablet layout
- [ ] build out laptop layout
- [ ] build out desktop layout
- [ ] start on javascript interactivity.
- [ ] get button to connect to needed property
- [ ] rerender operations.
- [ ] find out if i can return different fetch items. ? 